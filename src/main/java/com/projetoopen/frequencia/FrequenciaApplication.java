package com.projetoopen.frequencia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableWebMvc
public class FrequenciaApplication {

	public static void main(String[] args) {
		SpringApplication.run(FrequenciaApplication.class, args);
	}

}
