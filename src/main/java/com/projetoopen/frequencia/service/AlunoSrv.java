package com.projetoopen.frequencia.service;

import com.projetoopen.frequencia.entity.Aluno;
import com.projetoopen.frequencia.repository.AlunoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AlunoSrv {
    @Autowired
    AlunoRepository alunoRepository;

    @Transactional
    public void save(Aluno aluno) {
        this.alunoRepository.save(aluno);
    }

    public List<Aluno> findAll() {
        return this.alunoRepository.findAll();
    }
}
