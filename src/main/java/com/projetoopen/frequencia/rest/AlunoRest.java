package com.projetoopen.frequencia.rest;

import com.projetoopen.frequencia.entity.Aluno;
import com.projetoopen.frequencia.service.AlunoSrv;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping(value = "/aluno", produces = "application/json")
public class AlunoRest {

    @Autowired
    AlunoSrv alunoSrv;

    @RequestMapping(method = GET)
    public ResponseEntity<List<Aluno>> get() {
        return ok(alunoSrv.findAll());
    }

    @RequestMapping(method = POST)
    public ResponseEntity<Aluno> post(@RequestBody Aluno aluno) {
        alunoSrv.save(aluno);
        return new ResponseEntity<>(aluno, CREATED);
    }
}
