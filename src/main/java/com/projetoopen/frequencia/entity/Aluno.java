package com.projetoopen.frequencia.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
public class Aluno extends BaseEntity{

    private String nome;
    private String matricula;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
}
